#!/bin/bash

ARCH=$(dpkg --print-architecture)
HOSTRELEASE=$(cat /etc/os-release | grep VERSION_CODENAME | cut -d"=" -f2)

if [[ ${ARCH} != "arm64" || "buster bullseye bionic focal jammy" != *"$HOSTRELEASE"* ]]; then

	echo "You are running on an unsupported system"
	exit

fi

mkdir -p ~/.pip
cat <<EOF > ~/.pip/pip.conf
[global]
timeout = 6000
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
trusted-host = pypi.tuna.tsinghua.edu.cn
EOF

sudo apt-get update
sudo apt-get install -y python3-pip libopencv-dev \
python3-opencv imagemagick python3-scipy python3-setuptools python3-wheel \
python3-dev cmake python3-testresources

python3 -m pip install -U pip setuptools wheel

[[ ! -d python_whl ]] && git clone --depth=1 https://gitee.com/leeboby/python_whl
[[ ! -d python_whl ]] && exit
cd python_whl
case "$HOSTRELEASE" in
        bionic)
		python3 -m pip install dlib-19.24.0-cp36-cp36m-linux_aarch64.whl
                ;;
        focal)
		python3 -m pip install dlib-19.24.0-cp38-cp38-linux_aarch64.whl
                ;;
        jammy)
		python3 -m pip install dlib-19.24.0-cp310-cp310-linux_aarch64.whl
                ;;
        buster)
		python3 -m pip install dlib-19.24.0-cp37-cp37m-linux_aarch64.whl
                ;;
        bullseye)
		python3 -m pip install dlib-19.24.0-cp39-cp39-linux_aarch64.whl
                ;;
        *)
		echo "$HOSTRELEASE Error"
		exit
                ;;
esac
lib_ver=$(python3 -c "import dlib; print(dlib.__version__)")
[[ ${lib_ver} != 19.24.0 ]] && exit
python3 -m pip install face_recognition_models-0.3.0-py2.py3-none-any.whl

python3 -m pip install face_recognition
python3 -m pip install flask

[[ ! -d /root/face_recognition ]] && git clone --depth=1 https://gitee.com/leeboby/face_recognition.git ~/face_recognition
cd ~/face_recognition/examples
python3 find_faces_in_picture.py
python3 find_facial_features_in_picture.py
python3 identify_and_draw_boxes_on_faces.py
python3 face_distance.py
python3 recognize_faces_in_pictures.py
